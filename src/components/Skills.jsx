import data from "../assets/data.json";
const Skills = ({ language }) => {
    return (
        <>
            <section id="my-skills" className="ts-block pb-5">
                <div className="ts-title text-center">
                    <h2>{data.myskills[language]}</h2>
                </div>
                <div className="container">
                    <div className="row ">
                        <div className="col-lg-6">
                            <h4>{data.everyday[language]}</h4>
                            <p>
                                {data.everydayP1[language]}
                            </p>
                            <p>
                                {data.everydayP2[language]}
                            </p>
                            <a href="#contact" className="btn btn-outline-light mb-5">{data.contactMe[language]}</a>
                        </div>
                        <div className="col-lg-6">
                            <div className="row text-center">
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/phpLogo.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Php</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/sf.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Symfony</h5>
                                </div>

                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/html.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Html</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/css.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Css</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/bootstrap.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Bootstrap</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/mysql.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">MySql</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/js.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Javascript</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/react.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">React</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/java.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Java</h5>
                                </div>
                                <div className="col-6 col-lg-3 col-md-3 col-sm-6 mb-3">
                                    <figure className="icon">
                                        <img src="img/spring.png" alt="" />
                                    </figure>
                                    <h5 className="ts-progress-title">Spring Boot</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default Skills;